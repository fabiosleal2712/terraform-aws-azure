variable "portas" {
  description = "Portas abertas"
  type        = list(number)
  default     = [22, 80, 444, 8080, 3606]
}