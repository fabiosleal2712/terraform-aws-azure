variable "cidr_vpc" {
  description = "Cidr Block VPC"
  type        = string
}

variable "cidr_subnet" {
  description = "Cidr Block subne"
  type        = string
}

variable "environment" {
  description = "Ambiente onde o recurso vai ser ultilizado"
  type        = string
}